# Desafio 01 - Fundamentos

Projeto destinado na resolução do **Desafio 01 - Fundamentos: To do List**.

![Desafio elaborado pela Rocketseat](/docs/images/cover_challengue.png)

Conforme ilustrado na imagem acima, trata-se de um controle de tarefas no estilo _to-do list_, que disponibilizar as
seguintes funcionalidades:

- Adicionar uma nova tarefa
- Marcar e desmarcar uma tarefa como concluída
- Remover uma tarefa da listagem
- Mostrar o progresso de conclusão das tarefas

E entende-se por `tarefa` um texto descritivo de até **70 caracteres** (restrição extra à expecificação inicial) que pode
estar finalizada ou por fazer.

A interface de usuário foi fornecida, via projeto Figma, na especificação e esta que foi implementada neste projeto.
