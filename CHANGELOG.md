<!-- markdownlint-disable MD024 -->
# Changelog

> Como lembrete: [Changelog vs. Release Notes: Differences and Examples](https://blog.releasenotes.io/changelog-vs-release-notes/)

Todas as distintas mudanças que ocorrem com este projeto serão registradas neste arquivo.

O formato está baseado em [Mantenha um Changelog](https://keepachangelog.com/pt-BR/1.0.0/),
e este projeto segue o [Versionamento Semântico 2.0.0](https://semver.org/lang/pt-BR/).

## [Exemplo] - YYYY-MM-DD

> [Comparação completa]

### Adicionado

para novos recursos.

### Alterado

para alterações na funcionalidade existente.

### Depreciado

para recursos que serão removidos em breve.

### Removido

por recursos removidos.

### Consertado

para qualquer correção de bug.

### Segurança

em caso de vulnerabilidades.

## [Em andamento]

### Adicionado

- Tela inicial da aplicação
- Renderização de listagem sem tarefas incluídas
- Inclusão de tarefa à listagem
- Marcar e desmarcar uma tarefa como concluída
- Remover uma tarefa da listagem
