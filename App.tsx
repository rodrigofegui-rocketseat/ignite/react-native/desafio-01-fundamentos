import { useFonts } from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import { StatusBar } from 'expo-status-bar';
import { useCallback } from 'react';
import { StyleSheet, View } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import { Home } from '@app/scenes/Home';

SplashScreen.preventAutoHideAsync();

export default function App() {
    const [isLoaded] = useFonts({
        Inter: require('@assets/fonts/Inter.ttf'),
    });

    const hideSplashscreenWhenFonts = useCallback(async () => {
        if (isLoaded) {
            await SplashScreen.hideAsync();
        }
    }, [isLoaded]);

    if (!isLoaded) {
        return null;
    }

    return (
        <SafeAreaProvider>
            <StatusBar style='light' />

            <View
                style={styles.container}
                onLayout={hideSplashscreenWhenFonts}>
                <Home />
            </View>
        </SafeAreaProvider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
