import { Dimensions } from 'react-native';

export const sizing = {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
};

export const colors = {
    primary: '#1E6F9F',
    primaryHover: '#4EA8DE',
    secondary: '#5E60CE',
    secondaryHover: '#8284FA',
    error: '#E25858',
    dark: '#0D0D0D', // gray700,
    darkMuted: '#1A1A1A', // gray600
    gray: '#262626', // gray500
    lightGray: '#333333', // gray400
    lightGrayMuted: '#808080', // gray300
    lighterGray: '#D9D9D9', // gray200
    light: '#F2F2F2', // gray100
};
