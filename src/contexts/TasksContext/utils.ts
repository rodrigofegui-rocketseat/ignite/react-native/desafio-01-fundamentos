import uuid from 'react-native-uuid';

import { Task, Tasks } from '@app/types/Task';

export const areTasksEquals = (task1?: Task, task2?: Task): boolean => {
    if (!task1 && task2) return false;
    if (task1 && !task2) return false;

    return (
        task1?.id === task2?.id &&
        task1?.description === task2?.description &&
        task1?.createdAt === task2?.createdAt &&
        task1?.isDone === task2?.isDone
    );
};

export const doAdd = (
    description: string,
    tasks: Tasks,
    setTasks: React.Dispatch<React.SetStateAction<Tasks>>,
) => {
    if (_alreadyExist(tasks, description)) {
        throw new Error('Tarefa já existe!');
    }

    const task: Task = {
        id: uuid.v4() as string,
        createdAt: new Date(),
        isDone: false,
        description,
    };

    setTasks((previousTasks) => [...previousTasks, task]);
};

export const doToggleDone = (
    targetID: string,
    setTasks: React.Dispatch<React.SetStateAction<Tasks>>,
) => {
    setTasks((previousTasks) =>
        previousTasks.map((cTask) =>
            cTask.id !== targetID ? cTask : { ...cTask, isDone: !cTask.isDone },
        ),
    );
};

export const doRemove = (
    targetID: string,
    setTasks: React.Dispatch<React.SetStateAction<Tasks>>,
) => {
    setTasks((previousTasks) =>
        previousTasks.filter((cTask) => cTask.id !== targetID),
    );
};

export const getCreatedCnt = (tasks: Tasks): number => {
    return tasks.length;
};

export const getDoneCnt = (tasks: Tasks): number => {
    return tasks.filter((task) => task.isDone).length;
};

export const getSortedTasks = (tasks: Tasks): Tasks => {
    return tasks.sort((task1, task2) => {
        if (task1.isDone !== task2.isDone) {
            return Number(task1.isDone) - Number(task2.isDone);
        }

        return task1.createdAt.getTime() - task2.createdAt.getTime();
    });
};

const _alreadyExist = (tasks: Tasks, description: string): boolean => {
    return !!tasks.find((task) => task.description === description);
};
