import { ReactNode, createContext, useState } from 'react';

import { Tasks } from '@app/types/Task';

import {
    doAdd,
    doRemove,
    doToggleDone,
    getCreatedCnt,
    getDoneCnt,
    getSortedTasks,
} from './utils';

type TasksContextProviderProps = {
    children: ReactNode;
};

type TasksContextProps = {
    tasks: Tasks;
    createdCnt: number;
    doneCnt: number;
    addTask: (description: string) => void;
    toggleTaskAsDone: (targetID: string) => void;
    removeTask: (targetID: string) => void;
};

export const TasksContext = createContext<TasksContextProps>({
    tasks: [],
    createdCnt: 0,
    doneCnt: 0,
    addTask: () => ({}),
    toggleTaskAsDone: () => ({}),
    removeTask: () => ({}),
});

export const TasksContextProvider = ({
    children,
}: TasksContextProviderProps) => {
    const [rawTasks, setTasks] = useState<Tasks>([]);

    const createdCnt = getCreatedCnt(rawTasks);
    const doneCnt = getDoneCnt(rawTasks);
    const tasks = getSortedTasks(rawTasks);

    const addTask = (description: string) =>
        doAdd(description, rawTasks, setTasks);

    const toggleTaskAsDone = (targetID: string) =>
        doToggleDone(targetID, setTasks);

    const removeTask = (targetID: string) => doRemove(targetID, setTasks);

    return (
        <TasksContext.Provider
            value={{
                tasks,
                createdCnt,
                doneCnt,
                addTask,
                toggleTaskAsDone,
                removeTask,
            }}>
            {children}
        </TasksContext.Provider>
    );
};
