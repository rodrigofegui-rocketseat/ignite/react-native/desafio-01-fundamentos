export type Task = {
    id: string;
    description: string;
    createdAt: Date;
    isDone: boolean;
};

export type Tasks = Task[];
