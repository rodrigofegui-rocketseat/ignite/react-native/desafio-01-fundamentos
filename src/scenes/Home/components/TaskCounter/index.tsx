import { useContext } from 'react';
import { View, ViewProps } from 'react-native';

import { TasksContext } from '@app/contexts/TasksContext';
import { colors } from '@app/styles';

import { Counter } from './Counter';
import { styles } from './styles';

type TaskCounterProps = ViewProps;

export const TaskCounter = ({ style }: TaskCounterProps) => {
    const { createdCnt, doneCnt } = useContext(TasksContext);

    return (
        <View style={style}>
            <View style={styles.container}>
                <Counter
                    label='Criadas'
                    count={createdCnt}
                    color={colors.primaryHover}
                />
                <Counter
                    label='Concluídas'
                    count={doneCnt}
                    color={colors.secondaryHover}
                />
            </View>
        </View>
    );
};
