import { StyleSheet } from 'react-native';

import { colors } from '@app/styles';

export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    label: {
        fontSize: 14,
        fontWeight: 'bold',
    },
    count: {
        fontSize: 13,
        marginLeft: 5,
        paddingHorizontal: 8,
        paddingVertical: 'auto',
        borderRadius: 100,
        backgroundColor: colors.lightGray,
        color: colors.light,
    },
});
