import { View } from 'react-native';

import { Text } from '@app/components/ToDo';

import { styles } from './styles';

type CounterProps = {
    label: string;
    count: number;
    color?: string;
};

export const Counter = ({ label, count, color }: CounterProps) => {
    return (
        <View style={styles.container}>
            <Text style={{ ...styles.label, color }}>{label}</Text>
            <Text style={styles.count}>{count}</Text>
        </View>
    );
};
