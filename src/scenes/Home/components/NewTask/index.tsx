import { PlusCircle } from 'phosphor-react-native';
import { useContext } from 'react';
import { Alert, View } from 'react-native';

import { Input } from '@app/components/ToDo';
import { TasksContext } from '@app/contexts/TasksContext';

import { styles } from './styles';

export const NewTask = () => {
    const { addTask } = useContext(TasksContext);

    const handleAddTask = (description: string) => {
        try {
            addTask(description);
        } catch (error) {
            if (error instanceof Error) {
                Alert.alert(error.message);
            }
        }
    };

    return (
        <View style={styles.container}>
            <Input.Inline
                placeholder='Adicione uma nova tarefa'
                IconComponent={PlusCircle}
                onPress={handleAddTask}
            />
        </View>
    );
};
