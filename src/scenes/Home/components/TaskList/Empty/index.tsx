import { Notepad } from 'phosphor-react-native';
import { View } from 'react-native';

import { EmptySpace } from '@app/components/EmptySpace';
import { Separator } from '@app/components/Separator';
import { Text } from '@app/components/ToDo';
import { colors } from '@app/styles';

import { styles } from './styles';

export const Empty = () => {
    return (
        <>
            <Separator />

            <EmptySpace vertical={46} />

            <View style={styles.container}>
                <Notepad
                    weight='thin'
                    size={64}
                    color={colors.lightGrayMuted}
                />

                <EmptySpace vertical={10} />

                <Text style={styles.situation}>
                    Você ainda não têm tarefas cadastradas
                </Text>

                <Text style={styles.callToAction}>
                    Crie tarefas e organize seus itens a fazer
                </Text>
            </View>
        </>
    );
};
