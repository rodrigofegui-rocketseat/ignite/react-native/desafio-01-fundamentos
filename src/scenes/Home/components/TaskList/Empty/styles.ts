import { StyleSheet } from 'react-native';

import { colors } from '@app/styles';

export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        rowGap: 2,
    },
    situation: {
        color: colors.lightGrayMuted,
        fontWeight: 'bold',
    },
    callToAction: {
        fontSize: 12,
        color: colors.lightGrayMuted,
    },
});
