import { useContext } from 'react';
import { FlatList, View, ViewProps } from 'react-native';

import { TasksContext } from '@app/contexts/TasksContext';
import { Task } from '@app/types/Task';

import { Empty } from './Empty';
import { TaskItem } from './TaskItem';

type TaskListProps = ViewProps;

export const TaskList = ({ style }: TaskListProps) => {
    const { tasks } = useContext(TasksContext);

    const renderItem = ({ item, index }: { item: Task; index: number }) => (
        <TaskItem
            item={item}
            index={index}
        />
    );

    return (
        <View style={style}>
            <FlatList
                data={tasks}
                keyExtractor={(item) => item.id}
                renderItem={renderItem}
                ListEmptyComponent={Empty}
            />
        </View>
    );
};
