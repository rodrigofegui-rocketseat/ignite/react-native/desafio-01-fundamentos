import { StyleSheet } from 'react-native';

import { colors } from '@app/styles';

export const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.lightGray,
        borderColor: colors.lightGrayMuted,
        borderWidth: 0.5,
        borderRadius: 5,
        alignItems: 'stretch',
        flexDirection: 'row',
        marginVertical: 4,
        minHeight: 70,
    },
    description: {
        flex: 1,
        fontSize: 16,
        color: colors.light,
        textAlignVertical: 'center',
    },
    doneDescription: {
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: colors.light,
        fontStyle: 'italic',
        color: colors.lightGrayMuted,
    },
    action: {
        paddingHorizontal: 14,
    },
});
