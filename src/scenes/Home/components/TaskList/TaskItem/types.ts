import { IconProps, IconWeight } from 'phosphor-react-native';

export type IconizedProgress = {
    icon: React.FC<IconProps>;
    weight: IconWeight;
    color: string;
};
