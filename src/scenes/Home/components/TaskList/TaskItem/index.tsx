import { Trash } from 'phosphor-react-native';
import { memo, useContext } from 'react';
import { View } from 'react-native';

import { IconButton, Text } from '@app/components/ToDo';
import { TasksContext } from '@app/contexts/TasksContext';
import { areTasksEquals } from '@app/contexts/TasksContext/utils';
import { colors } from '@app/styles';
import { Task } from '@app/types/Task';

import { styles } from './styles';
import { getIconizedProgress } from './utils';

type TaskProps = {
    item?: Task;
    index: number;
};

const TaskItemComponent = ({ item: task }: TaskProps) => {
    const { toggleTaskAsDone, removeTask } = useContext(TasksContext);

    if (!task) return null;

    const iconizedProgress = getIconizedProgress(task);

    return (
        <View style={styles.container}>
            <IconButton
                IconComponent={iconizedProgress.icon}
                style={styles.action}
                weight={iconizedProgress.weight}
                color={iconizedProgress.color}
                isOutlined
                onPress={() => toggleTaskAsDone(task.id)}
            />

            <Text
                style={[
                    styles.description,
                    task.isDone ? styles.doneDescription : {},
                ]}>
                {task.description}
            </Text>

            <IconButton
                IconComponent={Trash}
                style={styles.action}
                color={colors.lightGrayMuted}
                btnStyle='danger'
                isOutlined
                onPress={() => removeTask(task.id)}
            />
        </View>
    );
};

const arePropsEqual = (oldProps: TaskProps, newProps: TaskProps) => {
    const areEquals = areTasksEquals(oldProps.item, newProps.item);

    return areEquals;
};

export const TaskItem = memo(TaskItemComponent, arePropsEqual);
