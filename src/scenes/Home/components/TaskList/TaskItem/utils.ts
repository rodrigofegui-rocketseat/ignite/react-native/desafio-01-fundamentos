import { CheckCircle, Circle } from 'phosphor-react-native';

import { colors } from '@app/styles';
import { Task } from '@app/types/Task';

import { IconizedProgress } from './types';

export const getIconizedProgress = (task: Task): IconizedProgress => {
    if (task.isDone) {
        return {
            icon: CheckCircle,
            weight: 'fill',
            color: colors.secondary,
        };
    }

    return {
        icon: Circle,
        weight: 'regular',
        color: colors.primaryHover,
    };
};
