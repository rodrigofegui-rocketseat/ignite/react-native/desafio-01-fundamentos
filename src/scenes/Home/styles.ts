import { StyleSheet } from 'react-native';

import { colors, sizing } from '@app/styles';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.darkMuted,
    },
    content: {
        flex: 1,
        width: sizing.width * 0.9,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: -24,
        marginLeft: 'auto',
        marginRight: 'auto',
        paddingBottom: 20,
    },
    spreedWidth: {
        width: '100%',
    },
    list: {
        flex: 1,
    },
});
