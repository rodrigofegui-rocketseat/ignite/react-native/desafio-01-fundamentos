import { View } from 'react-native';

import { EmptySpace } from '@app/components/EmptySpace';
import { Logo } from '@app/components/Logo';
import { TasksContextProvider } from '@app/contexts/TasksContext';

import { NewTask, TaskCounter, TaskList } from './components';
import { styles } from './styles';

export const Home = () => {
    return (
        <View style={styles.container}>
            <Logo.Panel />

            <TasksContextProvider>
                <View style={styles.content}>
                    <NewTask />

                    <EmptySpace vertical={28} />

                    <TaskCounter style={styles.spreedWidth} />

                    <EmptySpace vertical={20} />

                    <TaskList
                        style={{ ...styles.spreedWidth, ...styles.list }}
                    />
                </View>
            </TasksContextProvider>
        </View>
    );
};
