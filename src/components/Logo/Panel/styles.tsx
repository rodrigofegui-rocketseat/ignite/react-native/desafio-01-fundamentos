import { StyleSheet } from 'react-native';

import { colors, sizing } from '@app/styles';

export const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.dark,
        height: sizing.height * 0.21,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        aspectRatio: 3.45, // original width / original height => 3539 / 1027 ~= 3.45
        width: sizing.width * 0.35,
    },
});
