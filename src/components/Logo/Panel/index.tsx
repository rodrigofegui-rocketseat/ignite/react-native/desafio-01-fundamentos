import { Image } from 'expo-image';
import { View } from 'react-native';

import { styles } from './styles';

export const Panel = () => {
    return (
        <View style={styles.container}>
            <Image
                style={styles.image}
                source={require('@assets/logo.png')}
                contentFit='contain'
            />
        </View>
    );
};
