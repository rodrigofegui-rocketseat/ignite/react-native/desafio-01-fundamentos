import { colors } from '@app/styles';

import { ButtonStyle, ColorScheme } from './types';

export const getColorScheme = (btnStyle: ButtonStyle): ColorScheme => {
    if (['primary', 'secondary'].includes(btnStyle)) {
        return {
            bgColor: colors[btnStyle as keyof typeof colors],
            hoverColor: colors[`${btnStyle}Hover` as keyof typeof colors],
        };
    } else if (['danger'].includes(btnStyle)) {
        return {
            bgColor: colors.error,
            hoverColor: colors.error,
        };
    }

    return {
        bgColor: 'transparent',
        hoverColor: 'transparent',
    };
};
