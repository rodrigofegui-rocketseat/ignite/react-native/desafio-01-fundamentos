import { StyleSheet } from 'react-native';

import { colors } from '@app/styles';

export const styles = StyleSheet.create({
    container: {
        borderRadius: 5,
    },
    button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.primary,
        paddingHorizontal: 18,
        borderRadius: 5,
        borderColor: colors.dark,
    },
});
