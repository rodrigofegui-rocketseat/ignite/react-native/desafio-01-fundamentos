import { IconProps } from 'phosphor-react-native';
import { FC } from 'react';
import {
    TouchableHighlight,
    TouchableHighlightProps,
    View,
} from 'react-native';

import { colors } from '@app/styles';

import { styles } from './styles';
import { ButtonStyle } from './types';
import { getColorScheme } from './utils';

export type IconButtonProps = Omit<TouchableHighlightProps, 'onPress'> &
    IconProps & {
        IconComponent: FC<IconProps>;
        btnStyle?: ButtonStyle;
        isOutlined?: boolean;
        isBorded?: boolean;
        onPress?: (a: string) => void;
    };

export const IconButton = ({
    IconComponent,
    color = colors.light,
    weight = 'regular',
    btnStyle = 'primary',
    isOutlined = false,
    isBorded = false,
    style,
    onPress,
}: IconButtonProps) => {
    const { bgColor, hoverColor } = getColorScheme(btnStyle);

    const finalBgColor = isOutlined ? 'transparent' : bgColor;
    const finalHoverColor = isOutlined ? bgColor : hoverColor;

    return (
        <TouchableHighlight
            style={styles.container}
            activeOpacity={0.6}
            underlayColor={finalHoverColor}
            onPress={onPress}>
            <View
                style={[
                    styles.button,
                    {
                        backgroundColor: finalBgColor,
                        borderWidth: isBorded ? 1 : 0,
                    },
                    style,
                ]}>
                <IconComponent
                    size={20}
                    color={color}
                    weight={weight}
                />
            </View>
        </TouchableHighlight>
    );
};
