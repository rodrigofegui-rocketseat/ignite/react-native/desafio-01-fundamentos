export type ButtonStyle = 'primary' | 'secondary' | 'danger' | 'link';

export type ColorScheme = {
    bgColor: string;
    hoverColor: string;
};
