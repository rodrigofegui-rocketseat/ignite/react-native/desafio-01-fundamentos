import { Text as NativeText, TextProps as NativeTextProps } from 'react-native';

import { styles } from './styles';

type CustomTextProps = NativeTextProps;
export const Text = ({ children, ...others }: CustomTextProps) => {
    return (
        <NativeText
            style={styles.text}
            {...others}>
            {children}
        </NativeText>
    );
};
