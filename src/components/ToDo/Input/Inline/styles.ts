import { StyleSheet } from 'react-native';

import { colors } from '@app/styles';

export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    input: {
        fontFamily: 'Inter',
        flex: 1,
        marginRight: 8,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: colors.lightGray,
        paddingVertical: 10,
        paddingHorizontal: 16,
        color: colors.light,
    },
});
