import { useRef, useState } from 'react';
import { TextInput, TextInputProps, View } from 'react-native';

import { colors } from '@app/styles';

import { IconButton, IconButtonProps } from '../../IconButton';

import { styles } from './styles';

type InlineProps = TextInputProps & IconButtonProps;

export const Inline = ({
    IconComponent,
    btnStyle,
    onPress,
    ...originalProps
}: InlineProps) => {
    const [value, setValue] = useState<string>('');
    const inputRef = useRef<TextInput>(null);

    const handleFocusInOut = () => {
        inputRef.current?.setNativeProps({
            style: {
                borderColor: inputRef.current?.isFocused()
                    ? colors.secondaryHover
                    : colors.dark,
            },
        });
    };

    const onIconPress = () => {
        if (!value) return;

        onPress(
            value ||
                'Integer urna interdum massa libero auctor neque turpis turpis semper.',
        );

        setValue('');
    };

    return (
        <View style={styles.container}>
            <TextInput
                ref={inputRef}
                style={styles.input}
                placeholderTextColor={colors.lightGrayMuted}
                inputMode='text'
                maxLength={70}
                value={value}
                onChangeText={setValue}
                onFocus={handleFocusInOut}
                onBlur={handleFocusInOut}
                {...originalProps}
            />
            <IconButton
                IconComponent={IconComponent}
                btnStyle={btnStyle}
                onPress={onIconPress}
                isBorded
                {...originalProps}
            />
        </View>
    );
};
