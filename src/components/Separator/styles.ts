import { StyleSheet } from 'react-native';

import { colors, sizing } from '@app/styles';

export const styles = StyleSheet.create({
    container: {
        width: sizing.width,
        borderBottomWidth: 1.5,
        borderBottomColor: colors.lightGray,
    },
});
