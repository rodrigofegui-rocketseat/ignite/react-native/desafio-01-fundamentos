import { StyleProp, View, ViewStyle } from 'react-native';

type EmptySpaceProps = {
    horizontal?: number;
    vertical?: number;
};

export const EmptySpace = ({ horizontal, vertical }: EmptySpaceProps) => {
    const spacing: StyleProp<ViewStyle> = {};

    if (horizontal) {
        spacing.width = horizontal;
    }

    if (vertical) {
        spacing.height = vertical;
    }

    return <View style={spacing} />;
};
